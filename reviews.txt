======= Review 1 =======

> *** Submission Policy: Does the paper list the same author(s), title and abstract in its PDF file and EDAS registration?

yes.

> *** Strong aspects: Comments to the author: what are the strong aspects of the paper

Pretty detailed description of the methods and steps to analyze the traffic data.

> *** Weak aspects: Comments to the author: what are the weak aspects of the paper?

Results obtained in the paper are mostly straightforward and well known.

> *** Recommended changes: Recommended changes. Please indicate any changes that should be made to the paper if accepted.

1. in the abstract, "on week-long" --> "on the week-long";
2. in section II.B, "oblique protocol type" --> "particular protocol type";
3. in section III.A, should be "point of view";
4. in section III.A, below eq. (1), should be "discretized";
5. in section III.A, below eq. (1), "centroids" --> "centers";
6. in section III.A, one missing bracket in $E[Y(s,t)$;
7. in section III.B, "with an extreme goal" --> "with an ultimate goal";
8. in section III.B, "functions is positive definiteness" --> "functions to be positive definite";
9. in section III.B, "leveraging two kinds of" --> "leveraging on two kinds of";
10. above eq. (4), should be "monotonic";
11. eq. (6) missing power of 2;
12. in section IV.A, please elaborate more on the "goodness-of-fit statistics";
13. in section V.C, "plausible" to "more plausible";
14. in section VI, what is the meaning of "semantic knowledge of human mobility"?
15. in seciton VI, better change word "Fruits" to something else.

> *** Relevance and timeliness: Rate the importance and timeliness of the topic addressed in the paper within its area of research.
Acceptable (3)

> *** Technical content and scientific rigour: Rate the technical content of the paper (e.g.: completeness of the
analysis or simulation study, thoroughness of the treatise, accuracy of the models, etc.), its soundness and scientific rigour.
Valid work but limited contribution. (3)

> *** Novelty and originality: Rate the novelty and originality of the ideas or results presented in the paper.
Some interesting ideas and results on a subject well investigated. (3)

> *** Quality of presentation: Rate the paper organization, the clearness of text and figures, the completeness and accuracy of references.
Readable, but revision is needed in some parts. (3)

======= Review 2 =======

> *** Submission Policy: Does the paper list the same author(s), title and abstract in its PDF file and EDAS registration?

Yes

> *** Strong aspects: Comments to the author: what are the strong aspects of the paper

The authors perform a data-driven analysis and modeling of large-scale cellular traffic, focusing on the spatio-temporal dependence of traffic. The discovered long-range spatial dependence and detected tide effect in time across multiple regions can benefit the design of next-generation mobile networks. Also, the statistical modeling with both separable and non-separable models shows its usefulness in network management (e.g., prediction) or simulation researches. The potential threads that deserve further discusses about traffic dependence are the impact of the city's organization and real network topology in the observed urban areas, which will enhance the authors' conclusion about the difference between network structural cause and human-activity cause argued in the paper. The paper is well-written with clear structure and detailed description. Their analysis and model for cellular traffic problem will benefit multiple research and development areas such as network deploying and simulation researches.

> *** Weak aspects: Comments to the author: what are the weak aspects of the paper?

There are a few grammatical errors and typos in the draft.

> *** Recommended changes: Recommended changes. Please indicate any changes that should be made to the paper if accepted.

Please check the grammatical errors and typos.

> *** Relevance and timeliness: Rate the importance and timeliness of the topic addressed in the paper within its area of research.
Excellent (5)

> *** Technical content and scientific rigour: Rate the technical content of the paper (e.g.: completeness of the
analysis or simulation study, thoroughness of the treatise, accuracy of the models, etc.), its soundness and scientific rigour.
Excellent work and outstanding technical content. (5)

> *** Novelty and originality: Rate the novelty and originality of the ideas or results presented in the paper.
Significant original work and novel results. (4)

> *** Quality of presentation: Rate the paper organization, the clearness of text and figures, the completeness and accuracy of references.
Well written. (4)

======= Review 3 =======

> *** Submission Policy: Does the paper list the same author(s), title and abstract in its PDF file and EDAS registration?

yes

> *** Strong aspects: Comments to the author: what are the strong aspects of the paper

This paper explored the spatio-temporal dependence of cellular traffic at city scale. The main contributions are: 1) Large-scale analysis is performed in a week-long time with large population. 2) Both space dimension and time dimension are considered when modeling traffic dependence. 3) Research can benefit network deploying and simulation.The analysis presented in the paper seems to be valid. This paper is logically organized and clearly presented.

> *** Weak aspects: Comments to the author: what are the weak aspects of the paper?

However, the paper might be improved:
1)      Section 1 can be a little bit shortened while author can introduce more clearly about spatio-temporal covariance function(STCF) in Section 3, and also introduce more about analysis results in Section 5.
2)      Suggested that author can re-write the conclusion part that emphasis more on the result and discuss more about its application.
3)      In the description of Fig.5, the explanation of solid dots should put in the main text, and explain more clearly about the figure.

> *** Recommended changes: Recommended changes. Please indicate any changes that should be made to the paper if accepted.

1)      Section 1 can be a little bit shortened while author can introduce more clearly about spatio-temporal covariance function(STCF) in Section 3, and also introduce more about analysis results in Section 5.
2)      Suggested that author can re-write the conclusion part that emphasis more on the result and discuss more about its application.
3)      In the description of Fig.5, the explanation of solid dots should put in the main text, and explain more clearly about the figure.

> *** Relevance and timeliness: Rate the importance and timeliness of the topic addressed in the paper within its area of research.
Good (4)

> *** Technical content and scientific rigour: Rate the technical content of the paper (e.g.: completeness of the
analysis or simulation study, thoroughness of the treatise, accuracy of the models, etc.), its soundness and scientific rigour.
Valid work but limited contribution. (3)

> *** Novelty and originality: Rate the novelty and originality of the ideas or results presented in the paper.
Some interesting ideas and results on a subject well investigated. (3)

> *** Quality of presentation: Rate the paper organization, the clearness of text and figures, the completeness and accuracy of references.
Readable, but revision is needed in some parts. (3)

======= Review 4 =======

> *** Submission Policy: Does the paper list the same author(s), title and abstract in its PDF file and EDAS registration?

Yes

> *** Strong aspects: Comments to the author: what are the strong aspects of the paper

Despite recent progress in revealing temporal dynamics and spatial inhomogeneity of cellular traffic, limited knowledge about traffic dependence is gained. One of challenges comes from the absence of sustained observations at a network-wide scale. In the paper, the authors make an analysis on week-long traffic generated by a large population of users in a city of China, and model traffic dependence along both space and time dimensions. The evaluation results suggest connections between spatio-temporal dependence of cellular traffic and the organization of human lives. Region differences are observed to impact traffic dependence to a great extent. Additionally, interactive knowledge between space and time enhances traffic prediction with a decrease in root-mean-square error of 2.8%∼25.2%.

The paper is well written and organized. The experiment results are interesting and can be used for other city-scale research.

> *** Weak aspects: Comments to the author: what are the weak aspects of the paper?

Since it is a paper based on experimental experience, the authors are expected to provide more details on how to set up the experiments to collect the spatial-temporal data, and how to obtain and process the cellular traffic data in a week-long trace.

> *** Recommended changes: Recommended changes. Please indicate any changes that should be made to the paper if accepted.

Since it is a paper based on experimental experience, the authors are expected to provide more details on how to set up the experiments to collect the spatial-temporal data, and how to obtain and process the cellular traffic data in a week-long trace.

> *** Relevance and timeliness: Rate the importance and timeliness of the topic addressed in the paper within its area of research.
Good (4)

> *** Technical content and scientific rigour: Rate the technical content of the paper (e.g.: completeness of the
analysis or simulation study, thoroughness of the treatise, accuracy of the models, etc.), its soundness and scientific rigour.
Solid work of notable importance. (4)

> *** Novelty and originality: Rate the novelty and originality of the ideas or results presented in the paper.
Significant original work and novel results. (4)

> *** Quality of presentation: Rate the paper organization, the clearness of text and figures, the completeness and accuracy of references.
Well written. (4)
