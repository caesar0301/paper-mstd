#!/bin/bash
cd `dirname $0`/src
pdflatex -shell-escape mstd
bibtex mstd
pdflatex -shell-escape mstd
pdflatex -shell-escape mstd
cd -
exit 0;
